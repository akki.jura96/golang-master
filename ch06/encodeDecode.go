package main

// type UserAll struct {
// 	Name    string `json:"username"`
// 	Surname string `json:"surname"`
// 	Year    int    `json:"created"`
// }

// func main() {
// 	userall := UserAll{Name: "Mike", Surname: "Tsoukalos", Year: 2021}
// 	//кодирование данных JSON преобразовать структуру Go
// 	t, err := json.Marshal(&userall)
// 	if err != nil {
// 		fmt.Println(err)
// 	} else {
// 		fmt.Printf("Value %s\n", t)
// 	}

// 	//декодирование данных JSON
// 	str := `{"username":"M","surname": "Ts","created":2020}`
// 	jsonRecord := []byte(str)
// 	temp := UserAll{}
// 	err = json.Unmarshal(jsonRecord, &temp)
// 	if err != nil {
// 		fmt.Println(err)
// 	} else {
// 		fmt.Printf("Data type: %T with value %v\n", temp, temp)
// 	}
// }
