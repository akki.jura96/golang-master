package main

// type NoEmpty struct {
// 	Name    string `json:"username"`
// 	Surname string `json:"surname"`
// 	Year    int    `json:"creationyear,omitempty"` //Игнорирование пустых полей в JSON и не показывать пустые поля при маршале
// }
// type Password struct {
// 	Name    string `json:"username"`
// 	Surname string `json:"surname,omitempty"`
// 	Year    int    `json:"creationyear,omitempty"`
// 	Pass    string `json:"-"` //Удаление частных полей и не показывать эти поля при маршале
// }

// func main() {
// 	noempty := NoEmpty{Name: "Mihalis"}
// 	password := Password{Name: "Mihalis", Pass: "secret"}

// 	noEmptyVar, err := json.Marshal(&noempty)
// 	if err != nil {
// 		fmt.Println(err)
// 	} else {
// 		fmt.Printf("noEmptyVar decode with value %s\n", noEmptyVar)
// 	}

// 	passwordVar, err := json.Marshal(&password)
// 	if err != nil {
// 		fmt.Println(err)
// 	} else {
// 		fmt.Printf("passwordVar decode with valie %s\n", passwordVar)
// 	}
// }
