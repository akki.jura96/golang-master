package main

// func aliasNormalizeFunc(f *pflag.FlagSet, n string) pflag.NormalizedName {
// 	switch n {
// 	case "pass":
// 		n = "password"
// 		break
// 	case "ps":
// 		n = "password"
// 		break
// 	}
// 	return pflag.NormalizedName(n)
// }

// func main() {
// 	pflag.StringP("name", "n", "Mike", "Name parameter")
// 	pflag.StringP("password", "p", "hardtoGuess", "Password")
// 	pflag.CommandLine.SetNormalizeFunc(aliasNormalizeFunc)

// 	pflag.Parse()
// 	viper.BindPFlags(pflag.CommandLine) //привязывает существующий набор флагов к viper

// 	name := viper.GetString("name")
// 	password := viper.GetString("password")

// 	fmt.Println(name, password)

// 	//Reading an Environment variable
// 	viper.BindEnv("GOMAXPROCS")    //чтобы передать viper переменную среды
// 	val := viper.Get("GOMAXPROCS") //вызвав это мы можем прочитать ее значение
// 	if val != nil {
// 		fmt.Println("GOMAXPROCS first:", val)
// 	}

// 	//Setting an Environment variable
// 	viper.Set("GOMAXPROCS", 16) // изменить текущее значение переменной среды
// 	val = viper.Get("GOMAXPROCS")
// 	fmt.Println("GOMAXPROCS", val)
// }
