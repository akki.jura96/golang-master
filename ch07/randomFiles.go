package main

// func random(min, max int) int {
// 	return rand.Intn(max-min) + min
// }

// func create(file string) {
// 	//Does file already exists?
// 	_, err := os.Stat(file) //проверить информацию о файле по указанному пути
// 	if err == nil {
// 		fmt.Printf("%s already exists!\n", file)
// 		return
// 	}
// 	f, err := os.Create(file)
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// 	defer f.Close()

// 	lines := random(10, 30)
// 	for i := 0; i < lines; i++ {
// 		data := random(0, 20)
// 		fmt.Fprintf(f, "%d\n", data) //форматирует в соответствии со спецификатором формата и записывает в w. Возвращает количество записанных байтов
// 	}
// 	fmt.Printf("%s created!\n", file)
// }
// func main() {
// 	arguments := os.Args
// 	if len(arguments) != 5 {
// 		fmt.Println("Usage: randomFiles firstInt lastInt filename directory")
// 		return
// 	}
// 	start, err := strconv.Atoi(arguments[1])
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// 	end, err := strconv.Atoi(arguments[2])
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// 	if end < start {
// 		fmt.Println(end, "<", start)
// 		return
// 	}
// 	filename := arguments[3]
// 	path := arguments[4]
// 	// Does the destination directory exist?
// 	_, err = os.Open(path)
// 	if err != nil {
// 		fmt.Println(path, "does not exist!")
// 		return
// 	}

// 	var waitGroup sync.WaitGroup //ждет завершения набора горутин
// 	for i := start; i < end; i++ {
// 		waitGroup.Add(1)
// 		filepath := fmt.Sprintf("%s/%s%d", path, filename, i)
// 		go func(f string) {
// 			defer waitGroup.Done()
// 			create(f)
// 		}(filepath)
// 	}
// 	waitGroup.Wait()
// }
