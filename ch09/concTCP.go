package main

// var count = 0

// func handleConnection(c net.Conn) {
// 	fmt.Print(".")

// 	for {
// 		netData, err := bufio.NewReader(c).ReadString('\n') // read data from incoming request until \n and return string
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}
// 		temp := strings.TrimSpace(string(netData)) // removes spaces from given string
// 		if temp == "STOP" {
// 			break
// 		}
// 		fmt.Println(temp)
// 		counter := "Client bumber: " + strconv.Itoa(count) + "\n" // converts integer into string
// 		c.Write([]byte(string(counter)))                          // write to connection
// 	}
// 	c.Close()
// }

// func main() {
// 	arguments := os.Args
// 	if len(arguments) == 1 {
// 		fmt.Println("Please provide port number!")
// 		return
// 	}
// 	PORT := ":" + arguments[1]
// 	l, err := net.Listen("tcp4", PORT) // create tcp4 server with given port
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// 	defer l.Close()

// 	for {
// 		c, err := l.Accept() // waits and return connection
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}
// 		go handleConnection(c) // can work multiple client as paralell
// 		count++
// 	}
// }
