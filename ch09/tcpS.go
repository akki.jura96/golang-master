package main

// func main() {
// 	arguments := os.Args
// 	if len(arguments) == 1 {
// 		fmt.Println("Please provide port number")
// 		return
// 	}

// 	PORT := ":" + arguments[1]
// 	l, err := net.Listen("tcp", PORT) //create tcp server with given port
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// 	defer l.Close()

// 	c, err := l.Accept() //tpc server is open to connection and can accept request
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}

// 	for {
// 		netData, err := bufio.NewReader(c).ReadString('\n') //read from request until \n
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}
// 		if strings.TrimSpace(string(netData)) == "STOP" { // trim sent string with space and check STOP inside sent string
// 			fmt.Println("Exiting TCP server!")
// 			return
// 		}

// 		fmt.Print("-> ", string(netData)) // convert given data into string in order to show
// 		t := time.Now()                   // gives current local time
// 		myTime := t.Format(time.RFC3339) + "\n"
// 		c.Write([]byte(myTime)) // write to connection with bytes
// 	}
// }
