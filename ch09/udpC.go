package main

// func main() {
// 	arguments := os.Args
// 	if len(arguments) == 1 {
// 		fmt.Println("Please provide a host:port string!")
// 		return
// 	}
// 	CONNECT := arguments[1]
// 	s, err := net.ResolveUDPAddr("upd4", CONNECT) //make struct *net.UPDAddr with upd4 protocol
// 	c, err := net.DialUDP("upd4", nil, s)         // connecting to upd4 given server
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// 	fmt.Printf("The UPD server is %s\n", c.RemoteAddr().String()) //showing connecting remote server
// 	defer c.Close()                                               // close connection before return
// 	for {
// 		reader := bufio.NewReader(os.Stdin) // get text from console
// 		fmt.Printf(">> ")
// 		text, _ := reader.ReadString('\n') // read given text until \n
// 		data := []byte(text + "\n")        // converting into byte slice
// 		_, err = c.Write(data)             // write text to connection
// 		if strings.TrimSpace(string(data)) == "STOP" {
// 			fmt.Println("Exiting UDP client!")
// 			return
// 		}
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}
// 		buffer := make([]byte, 1024)       // making new byte slice with size 1024
// 		n, _, err := c.ReadFromUDP(buffer) // read value which returns remote server and put it into buffer
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}
// 		fmt.Printf("Reply: %s\n", string(buffer[0:n])) // showing returning value from remote server
// 	}
// }
