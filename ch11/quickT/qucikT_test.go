package quickt

import (
	"testing"
	"testing/quick"
)

// go test -v *.go -timeout 1s -count 2
// -timeout 1s - it stops tests after 1s as timeout
// -count 2 - it tests only given number (2)

var N = 1000000

func TestWithItself(t *testing.T) {
	condition := func(a, b Point2d) bool {
		return Add(a, b) == Add(b, a)
	}
	err := quick.Check(condition, &quick.Config{MaxCount: N})
	if err != nil {
		t.Errorf("Error: %v", err)
	}
}

func TestThree(t *testing.T) {
	condition := func(a, b, c Point2d) bool {
		return Add(Add(a, b), c) == Add(a, b)
	}
	err := quick.Check(condition, &quick.Config{MaxCount: N})
	if err != nil {
		t.Errorf("Error: %v", err)
	}
}
