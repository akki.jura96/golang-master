package quickt

type Point2d struct {
	X, Y int
}

func Add(x1, x2 Point2d) Point2d {
	temp := Point2d{}
	temp.X = x1.X + x2.X
	temp.Y = x1.Y + x2.Y
	return temp
}
