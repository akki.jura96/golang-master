package table

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
)

var BUFFERSIZE int
var FILESIZE int

// create random number between min and max
func random(min, max int) int {
	return rand.Intn(max-min) + min
}

// CreateBuffer makes slice type bytes with given len
// and writes random number into byte slice
func CreateBuffer(buf *[]byte, count int) {
	*buf = make([]byte, count) // make byte slice with given len -> count
	if count == 0 {
		return
	}
	// append random numbers to byte slice
	for i := 0; i < count; i++ {
		intByte := byte(random(50, 100))
		*buf = append(*buf, intByte)
	}
}

// create file with given path
func Create(dst string, b, filesize int) error {
	_, err := os.Stat(dst) // checks file is exists
	if err == nil {
		return fmt.Errorf("file %s already exists", dst)
	}
	f, err := os.Create(dst) // create file with given path
	if err != nil {
		fmt.Printf("error opening file %s", err)
		return err
	}
	defer f.Close()

	bwriter := bufio.NewWriterSize(f, b) // returns new writer with given size
	if err != nil {
		return err
	}
	buf := make([]byte, 0) // make byte slice with len 0
	CreateBuffer(&buf, b)  // return byte slice with random numbers with given len b

	for {
		_, err := bwriter.Write(buf)
		if err != nil {
			return err
		}
		if filesize < 0 {
			break
		}
		filesize = filesize - len(buf)
	}
	return err
}

func CountChars(filename string, b int) int {
	buf := make([]byte, 0)
	CreateBuffer(&buf, b)

	f, err := os.Open(filename)
	if err != nil {
		fmt.Printf("error opening file %s", err)
		return -1
	}
	defer f.Close()

	size := 0
	for {
		n, err := f.Read(buf) //reads number of bytes from the file and stores it into byte slice buf
		size = size + n
		if err != nil {
			break
		}
	}
	return size
}
