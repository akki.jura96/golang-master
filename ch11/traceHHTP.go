package main

// func main() {
// 	if len(os.Args) != 2 {
// 		fmt.Printf("Usage: URL\n")
// 		return
// 	}
// 	URL := os.Args[1]
// 	client := http.Client{}
// 	req, _ := http.NewRequest(http.MethodGet, URL, nil)

// 	trace := &httptrace.ClientTrace{
// 		GotFirstResponseByte: func() {
// 			fmt.Println("First response byte!")
// 		},
// 		GotConn: func(connInfo httptrace.GotConnInfo) {
// 			fmt.Printf("Got Conn: %+v\n", connInfo)
// 		},
// 		DNSDone: func(dnsInfo httptrace.DNSDoneInfo) {
// 			fmt.Printf("DNS Info: %+v\n", dnsInfo)
// 		},
// 		ConnectStart: func(network, addr string) {
// 			fmt.Println("Dial start")
// 		},
// 		ConnectDone: func(network, addr string, err error) {
// 			fmt.Println("Dial done")
// 		},
// 		WroteHeaders: func() {
// 			fmt.Println("Wrote headers")
// 		},
// 	}
// 	req = req.WithContext(httptrace.WithClientTrace(req.Context(), trace))
// 	fmt.Println("Requestingcdata from server")
// 	_, err := http.DefaultTransport.RoundTrip(req)
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// 	_, err = client.Do(req)
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// }
